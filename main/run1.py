from logManager import LogManager
from evaluator import UserEvaluator
from evaluator import CommitEvaluator
import nltk

# use to install packages for nltk via GUI:
# 1.) punkt
# 2.) averaged_perceptron_tagger
# nltk.download()
manager = LogManager()
#directory = "infinica_5_0"
# directory = "prom"
# directory = "camunda"
# directory = "2048game"
# manager.createGitLog("https://github.com/camunda/camunda-bpm-platform.git", "camunda")
text = manager.loadLog("infinica_5")
# commits = manager.processGitLog(text)
# commits = manager.processSVNLog(text)
# commits = manager.processMercurialLog(text)
commits = manager.processMercurialLog2(text)
print(len(commits))
userEvaluator = UserEvaluator()

# Create a list of users from the list of commits
users = userEvaluator.createUsers(commits)

# Creates criteria for users
userEvaluator.processUsers(users)
# Print basic criteria
for key, value in users.items():
    print(value.username + " add: " + str(len(value.additions)))
    print(value.username + " mod: " + str(len(value.modifications)))
    print(value.username + " del: " + str(len(value.deletions)))
#    print(value.username + " avg length: " + str(value.lengthAvg))
#    print(value.username + " frequency: " + str(value.frequency))
#    print(value.username + " usage test: " + str(value.wordcounts["Test"] / value.totalCommits))
#    print(value.username + " timeframe: " + str(value.timeframe))

# Print word count criteria
for key, value in users.items():
    for word, count in value.wordcounts.items():
        print(value.username + " wordcount " + word + ": " + str(value.wordcounts.get(word)))

# Do a k-means clustering of the users
#clusters = userEvaluator.kMeansUserClustering(users)
clusters = userEvaluator.kMeansUserClusteringLabels(users)
#clusters = userEvaluator.kMeansUserClusteringSimple(users)
