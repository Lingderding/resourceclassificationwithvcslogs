import subprocess
import os
from datetime import datetime

class LogManager:

    def createGitLog(self, url, directory):
        path = os.getcwd() + "/repositories/" + directory
        command = "git clone " + " " + url + " " + path
        subprocess.run(command, shell=True)
        process = subprocess.Popen("git log > log.txt --name-status", cwd=path, shell=True)
        process.communicate()

    def loadLog(self, path):
        with open("logs/" + path + ".txt", "r") as myfile:
            data = myfile.read()
            return data

    def processGitLog(self, text):
        text = "\n" + text
        rawCommits = text.split("\ncommit ")
        x = []
        commits = []
        for rawCommit in rawCommits:
            lines = rawCommit.split("\n")
            commit = Commit(lines[0].strip())
            for line in lines:
                if line.startswith("Author:"):
                    commit.setAuthor(line.replace("Author:", "").strip())
                elif line.startswith("Date:"):
                    format = "%a %b %d %H:%M:%S %Y %z"
                    commit.setTimestamp(self.createTimestamp(line.replace("Date:", "").strip(), format))
                elif ("related to " in line) or ("#CAM-" in line):
                    pass
                elif line.startswith("   "):
                    message = line.strip()
                    commit.addMessage(message)
                    commit.merge = message.startswith("Merge") or message.startswith("merge")
                elif line.startswith("A\t") and not commit.merge:
                    fragments = line.split(".")
                    commit.additions.append(fragments[len(fragments)-1])
                elif line.startswith("M\t") and not commit.merge:
                    fragments = line.split(".")
                    commit.modifications.append(fragments[len(fragments)-1])
                elif line.startswith("D\t") and not commit.merge:
                    fragments = line.split(".")
                    commit.deletions.append(fragments[len(fragments)-1])
            if hasattr(commit, 'author'):
                commits.append(commit)
        return commits

    def processSVNLog(self, text):
        text = "\n" + text
        rawCommits = text.split("\nRevision: ")
        commits = []
        for rawCommit in rawCommits:
            lines = rawCommit.split("\n")
            commit = Commit(lines[0].strip())
            for i in range(0, len(lines)-1):
                line = lines[i]
                if line.startswith("Author:"):
                    commit.setAuthor(line.replace("Author:", "").strip())
                elif line.startswith("Date:"):
                    format = "%A, %d. %B %Y %H:%M:%S"
                    commit.setTimestamp(self.createTimestamp(line.replace("Date:", "").strip(), format))
                elif line.startswith("Message:"):
                    index = i
                    while (index != -1):
                        index += 1
                        if "----" in lines[index]:
                            index = -1
                            commit.merge = commit.message.startswith("#merge") or commit.message.startswith("Merge")
                        elif lines[index] == "":
                            pass
                        else:
                            commit.addMessage(lines[index].strip())
                elif line.startswith("Modified : ") and not commit.merge:
                    fragments = line.split(".")
                    commit.modifications.append(fragments[len(fragments)-1])
                elif line.startswith("Added : ") and not commit.merge:
                    fragments = line.split(".")
                    commit.additions.append(fragments[len(fragments)-1])
                elif line.startswith("Deleted : ") and not commit.merge:
                    fragments = line.split(".")
                    commit.deletions.append(fragments[len(fragments)-1])
            if hasattr(commit, 'author'):
                commits.append(commit)
        return commits

    def processMercurialLog(self, text):
        lines = text.split("\n")
        commits = []
        for i in range(0, len(lines)-1):
            line = lines[i]
            if (line.startswith(" ")) or (line == ""):
                pass
            else:
                parts = line.split("   ")
                commit = Commit(parts[0].strip() + " " + parts[1].strip())
                format = "%Y-%m-%d %H:%M %z"
                commit.setTimestamp(self.createTimestamp(parts[2].strip(), format))
                commit.setAuthor(parts[3].strip())
                index = i
                while (index != -1):
                    index += 1
                    if len(lines[index]) < 2:
                        index = -1
                    elif lines[index] == "":
                        pass
                    else:
                        commit.addMessage(lines[index].strip())
                commits.append(commit)
        return commits

    def processMercurialLog2(self, text):
        commits = []
        text = "\n" + text
        commitLines = text.split("\n+++++ ")
        for commitText in commitLines:
            if commitText.strip() == "":
                continue
            parts = commitText.split(" ##### ")
            commit = Commit(parts[0] + " " + parts[1])
            format = "%Y-%m-%d %H:%M %z"
            commit.setTimestamp(self.createTimestamp(parts[2], format))
            commit.setAuthor(parts[3])
            messageLines = parts[4].split("\n")
            for line in messageLines:
                commit.addMessage(line)
            commit.merge = commit.message.startswith("Merge") or commit.message.startswith("merge")
            if not commit.merge:
                self.addToList(commit.additions, parts[5])
                self.addToList(commit.deletions, parts[6])
                self.addToList(commit.modifications, parts[7])
            commits.append(commit)

        return commits

    def createTimestamp(self, string, format):
        return datetime.strptime(string, format)

    def addToList(self, list, text):
        if text.strip() != "":
            files = text.split(" ")
            for file in files:
                parts = file.split(".")
                list.append(parts[len(parts)-1])


class Commit:
    def __init__(self, id):
        self.id = id
        self.message = ""
        self.merge = False
        self.additions = []
        self.modifications = []
        self.deletions = []

    def setAuthor(self, author):
        self.author = author

    def setTimestamp(self, timestamp):
        self.timestamp = timestamp

    def addMessage(self, message):
        self.message += message + " "