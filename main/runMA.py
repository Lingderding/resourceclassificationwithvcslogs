from logManager import LogManager
from evaluator import UserEvaluator
from evaluator import CommitEvaluator
import nltk

# use to install packages for nltk via GUI:
# 1.) punkt
# 2.) averaged_perceptron_tagger
# nltk.download()
manager = LogManager()

textInfinica = manager.loadLog("infinica_complete")
textProm = manager.loadLog("prom")
textCamunda = manager.loadLog("camunda")

commitsInfinica = manager.processMercurialLog2(textInfinica)
commitsProm = manager.processSVNLog(textProm)
commitsCamunda = manager.processGitLog(textCamunda)

#print("commits infinica: " + str(len(commitsInfinica)))
#print("commits prom: " + str(len(commitsProm)))
#print("commits camunda: " + str(len(commitsCamunda)))

userEvaluator = UserEvaluator()

# Create a list of users from the list of commits
usersInfinica = userEvaluator.createUsers(commitsInfinica)
usersProm = userEvaluator.createUsers(commitsProm)
usersCamunda = userEvaluator.createUsers(commitsCamunda)

print("\n")

#print("users infinica: " + str(len(usersInfinica)))
#print("users prom: " + str(len(usersProm)))
#print("users camunda: " + str(len(usersCamunda)))

# Creates criteria for users
userEvaluator.processUsers(usersInfinica)
userEvaluator.processUsers(usersProm)
userEvaluator.processUsers(usersCamunda)

# Print basic criteria
#for key, value in users.items():
#    print(value.username + " add: " + str(len(value.additions)))
#    print(value.username + " mod: " + str(len(value.modifications)))
#    print(value.username + " del: " + str(len(value.deletions)))
#    print(value.username + " avg length: " + str(value.lengthAvg))
#    print(value.username + " frequency: " + str(value.frequency))
#    print(value.username + " usage test: " + str(value.wordcounts["test"] / value.totalCommits))
#    print(value.username + " timeframe: " + str(value.timeframe))
#    print("\n")

# Print word count criteria
#for key, value in users.items():
#    for word, count in value.wordcounts.items():
#        print(value.username + " wordcount " + word + ": " + str(value.wordcounts.get(word)))
#    print("\n")

# Do a k-means clustering of the users (2 variables)
#clusters = userEvaluator.kMeansUserClusteringSimpleInfinica(usersInfinica)
#clusters = userEvaluator.kMeansUserClusteringSimpleProm(usersProm)
#clusters = userEvaluator.kMeansUserClusteringSimpleCamunda(usersCamunda)

# Create decisiontree
#decisiontree = userEvaluator.kMeansUserClusteringDecisionTreeInfinica(usersInfinica)
#decisiontree = userEvaluator.kMeansUserClusteringDecisionTreeProm(usersProm)
#decisiontree = userEvaluator.kMeansUserClusteringDecisionTreeCamunda(usersCamunda)

# Do a k-means clustering of the users (2 variables) and predict other data sets
clusters = userEvaluator.kMeansUserClusteringSimplePredict(usersInfinica, usersProm)