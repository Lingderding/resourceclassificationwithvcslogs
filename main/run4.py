from logManager import LogManager
from evaluator import UserEvaluator
from evaluator import CommitEvaluator
from evaluator import CommitType
from sklearn import tree
import matplotlib.pyplot as plt
import pandas as pd
from sklearn.tree import DecisionTreeClassifier, export_graphviz

# use to install packages for nltk via GUI:
# 1.) punkt
# 2.) averaged_perceptron_tagger
# nltk.download()
manager = LogManager()
#directory = "infinica_5_0"
# directory = "prom"
# directory = "camunda"
# directory = "2048game"
# manager.createGitLog("https://github.com/gabrielecirulli/2048.git", directory)
text = manager.loadLog("infinica_complete")
# commits = manager.processGitLog(text)
# commits = manager.processSVNLog(text)
# commits = manager.processMercurialLog(text)
commits = manager.processMercurialLog2(text)

commitEvaluator = CommitEvaluator()

#Counts word occurrences and prints the 50 most used
# counter = commitEvaluator.countWords(commits)
# print(counter.most_common(50))

typeMap = commitEvaluator.classifyCommits(commits)
# count = 0
# for key, value in typeMap.items():
#     print(repr(key) + ": " + str(value))
# unknownPercentage = typeMap[CommitType.other]/len(commits)
# print(unknownPercentage)

print("\n")

# for commit in commits:
#     if len(commit.types) == 0:
#         print(commit.message)

userEvaluator = UserEvaluator()
users = userEvaluator.createUsers(commits)

class Util:
    def getNumberFromMap(self, map, key):
        if key in map:
            return map[key]
        else:
            return 0

    def addUpMapNumbers(self, map, keys):
        sum = 0
        for key in keys:
            sum += self.getNumberFromMap(map, key)
        return sum

    def countTypes(self, types):
        sum = 0
        for key, value in types.items():
            if key != CommitType.merge and key != CommitType.other and key != CommitType.empty:
                sum += value
        return sum

# df = pd.DataFrame(columns=("name", "test", "development", "web", "documentation", "vcsManagement", "build", "toolManagement", "data", "design"))
# i = 0
# util = Util()
# for name, user in users.items():
#     count = util.countTypes(user.types)
#     if count == 0:
#         continue
#     test = util.getNumberFromMap(user.types, CommitType.test)/count * 100
#     development = util.addUpMapNumbers(user.types, [CommitType.development, CommitType.backend, CommitType.maintenance, CommitType.refactor])/count * 100
#     web = util.getNumberFromMap(user.types, CommitType.web)/count * 100
#     documentation = util.getNumberFromMap(user.types, CommitType.documentation)/count * 100
#     vcs = util.getNumberFromMap(user.types, CommitType.vcsManagement)/count * 100
#     build = util.getNumberFromMap(user.types, CommitType.build)/count * 100
#     tool = util.getNumberFromMap(user.types, CommitType.toolManagement)/count * 100
#     data = util.getNumberFromMap(user.types, CommitType.data)/count * 100
#     design = util.getNumberFromMap(user.types, CommitType.design)/count * 100
#     df.loc[i] = [user.username, test, development, web, documentation, vcs, build, tool, data, design]
#     i += 1
#
# pd.set_option('precision', 2)
# pd.set_option('expand_frame_repr', False)
# pd.set_option("display.max_rows",999)
# print(df)

with open("users/infinica2.txt", "r") as myfile:
    data = myfile.read()
lines = data.split("\n")

x = pd.DataFrame(columns=("test", "development", "web", "documentation", "vcsManagement", "build", "toolManagement", "data", "design"))
classes = pd.DataFrame(columns=("name", "class"))
i = 0
util = Util()
for line in lines:
    userclass = line.split("###")
    user = users[userclass[1]]
    classes.loc[i] = [userclass[1], userclass[0]]
    count = util.countTypes(user.types)
    if count == 0:
        continue
    test = util.getNumberFromMap(user.types, CommitType.test)/count * 100
    development = util.addUpMapNumbers(user.types, [CommitType.development, CommitType.backend, CommitType.maintenance, CommitType.refactor])/count * 100
    web = util.getNumberFromMap(user.types, CommitType.web)/count * 100
    documentation = util.getNumberFromMap(user.types, CommitType.documentation)/count * 100
    vcs = util.getNumberFromMap(user.types, CommitType.vcsManagement)/count * 100
    build = util.getNumberFromMap(user.types, CommitType.build)/count * 100
    tool = util.getNumberFromMap(user.types, CommitType.toolManagement)/count * 100
    data = util.getNumberFromMap(user.types, CommitType.data)/count * 100
    design = util.getNumberFromMap(user.types, CommitType.design)/count * 100
    x.loc[i] = [test, development, web, documentation, vcs, build, tool, data, design]
    i += 1

y = classes["class"]
clf = DecisionTreeClassifier()
model = clf.fit(x, y)
#model.predict([len(df['user']),2])
tree.export_graphviz(model, feature_names=["test", "development", "web", "documentation", "vcsManagement", "build", "toolManagement", "data", "design"],
                     class_names=["dev","support", "tester", "webdev"], filled=True, rounded=True)

# labels = []
# sizes = []
# possibleColors = ['yellowgreen', 'gold', 'lightskyblue', 'lightcoral', 'red', 'blue', 'green', 'yellow', 'brown', 'olive', 'orange', 'grey', 'violet', 'pink']
# colors = []
# explode = []
# types = users["developer senior mleitner"].types
# for typeName, count in types.items():
#     if typeName == CommitType.merge:
#         continue
#     labels.append(typeName)
#     sizes.append(count)
#     colors.append(possibleColors.pop())
#     explode.append(0)
#
# plt.pie(sizes, explode=explode, labels=labels, colors=colors,
#         autopct='%1.1f%%', shadow=True, startangle=90)
# # Set aspect ratio to be equal so that pie is drawn as a circle.
# plt.axis('equal')
#
# plt.show()


text = manager.loadLog("prom")
# commits = manager.processGitLog(text)
commits = manager.processSVNLog(text)

typeMap = commitEvaluator.classifyCommits(commits)
users = userEvaluator.createUsers(commits)

x = pd.DataFrame(columns=("test", "development", "web", "documentation", "vcsManagement", "build", "toolManagement", "data", "design"))
for name, user in users.items():

    count = util.countTypes(user.types)
    if count == 0:
        continue
    if len(user.timestamps)<5:
        continue
    test = util.getNumberFromMap(user.types, CommitType.test)/count * 100
    development = util.addUpMapNumbers(user.types, [CommitType.development, CommitType.backend, CommitType.maintenance, CommitType.refactor])/count * 100
    web = util.getNumberFromMap(user.types, CommitType.web)/count * 100
    documentation = util.getNumberFromMap(user.types, CommitType.documentation)/count * 100
    vcs = util.getNumberFromMap(user.types, CommitType.vcsManagement)/count * 100
    build = util.getNumberFromMap(user.types, CommitType.build)/count * 100
    tool = util.getNumberFromMap(user.types, CommitType.toolManagement)/count * 100
    data = util.getNumberFromMap(user.types, CommitType.data)/count * 100
    design = util.getNumberFromMap(user.types, CommitType.design)/count * 100
    x.loc[i] = [test, development, web, documentation, vcs, build, tool, data, design]
    i += 1

result = model.predict(x)
x['class'] = result
pd.set_option('precision', 2)
pd.set_option('expand_frame_repr', False)
pd.set_option("display.max_rows",999)
print(x)

total = 0
roles = {"dev": 0, "webdev": 0, "tester": 0, "support": 0}
for role in result:
    total+=1
    roles[role] += 1

print(total)
print(roles)