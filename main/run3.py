from logManager import LogManager
from evaluator import UserEvaluator
from evaluator import CommitEvaluator

# use to install packages for nltk via GUI:
# 1.) punkt
# nltk.download()
manager = LogManager()
#directory = "infinica"
# directory = "prom"
# directory = "camunda"
# directory = "2048game"
# manager.createGitLog("https://github.com/gabrielecirulli/2048.git", directory)
text = manager.loadLog("infinica")
# commits = manager.processGitLog(text)
# commits = manager.processSVNLog(text)
commits = manager.processMercurialLog(text)

commitEvaluator = CommitEvaluator()

#Counts word occurrences and prints the 50 most used which are not on the blacklist
counter = commitEvaluator.countWords(commits)
blacklist = ['.', 'Merge', 'with', 'the', ':', 'fo', 'to', '#', 'in', ',', 'and', 'of', 'a', ')', '(', 'porsche15', 'not', '-', 'from', 'is', 'on', '@', 'https', 'git-svn-id', 'merge', 'be', 'use', 'it', 'n\'t', 'xx', '\'\'', '\'', '``', 'Designer', 'WebDAV', 'ICR', 'text', 'porsche_mschedenig', '>', 'xx', 'xx', 'xx', 'xx', 'xx', 'xx',]
for common in counter.most_common(300):
    if commitEvaluator.occurrs(common[0], blacklist):
        continue
    print(common)


#typeCounts = commitEvaluator.classifyCommits(commits)
#for key, value in typeCounts.items():
    #print(repr(key) + ": " + str(value))