__author__ = 'Tono'

import nltk
from sklearn.cluster import KMeans
from sklearn import preprocessing
from sklearn import tree
from sklearn.tree import DecisionTreeClassifier, export_graphviz
import numpy as np
from pandas import DataFrame
import pandas as p
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from mpl_toolkits.mplot3d import proj3d
from enum import Enum
from nltk.stem.wordnet import WordNetLemmatizer
from nltk.tag import UnigramTagger
import pylab
from sklearn.externals.six import StringIO

import pydot
from IPython.display import Image
import os
from IPython.display import Image


class UserEvaluator:
    words = ["tests", "tested", "test", "testing", "fixed", "added", "automated", "case", "resource", "module", "bug", "elements", "error", "library", "function", "updated", "update", "encoding", "cleanup", "exception", "upgraded", "upgrade"]

    def countUserCommits(self, commits):
        counter = {}
        for commit in commits:
            if hasattr(commit, 'author'):
                if(commit.author in counter):
                    count = counter[commit.author]
                    counter[commit.author] = count+1
                else:
                    counter[commit.author] = 1
        return counter

    def createUsers(self, commits, users=None):
        if users == None:
            users = {}
        for commit in commits:
            if commit.author in users:
                user = users[commit.author]
            else:
                user = User(commit.author)
                users[commit.author] = user
            user.addMessage(commit.message)
            user.addTimestamp(commit.timestamp)
            user.addAdditions(commit.additions)
            user.addModifications(commit.modifications)
            user.addDeletions(commit.deletions)
            if hasattr(commit, 'types'):
                self.setTypeCount(commit.types, user)
        return users

    def setTypeCount(self, types, user):
        aggregatedTypes = []
        for type in types:
            if type == CommitType.addition or type == CommitType.removal:
                if len(types) == 1:
                    aggregatedTypes.append(type)
            else:
                aggregatedTypes.append(type)
        for type in aggregatedTypes:
            if type in user.types:
                user.types[type] = user.types[type] + 1
            else:
                user.types[type] = 1

    def processUsers(self, users):
        for key, value in users.items():
            value.totalCommits = len(value.timestamps)
            self.setTimeFrame(value)
            self.setFrequency(value)
            self.setAverageLength(value)
            self.setWordCounts(value)
            self.setAverageWordCounts(value)

    def setTimeFrame(self, user):
        user.timestamps.sort()
        timeframe = user.timestamps[len(user.timestamps)-1] - user.timestamps[0]
        user.timeframe = timeframe

    def setFrequency(self, user):
        month = user.timeframe.days/30
        if month != 0:
            user.frequency = len(user.timestamps)/month
        else:
            user.frequency = 0.01

    def setAverageLength(self, user):
        length = 0
        for message in user.messages:
            length += len(message)
        user.lengthAvg = length/len(user.messages)

    def setWordCounts(self, user):
        wordcounts = {}
        for word in self.words:
            wordcounts[word.lower()] = 0
        for message in user.messages:
            tokens = nltk.word_tokenize(message)
            words = [w.lower() for w in tokens]
            for word in self.words:
                count = words.count(word)
                #count = message.count(word)
                wordcounts[word] = count + wordcounts[word.lower()]
        user.wordcounts = wordcounts

    def setAverageWordCounts(self, user):
        wordcounts = {}
        for word in self.words:
            wordcounts[word.lower()] = 0
        for message in user.messages:
            tokens = nltk.word_tokenize(message)
            words = [w.lower() for w in tokens]
            for word in self.words:
                count = words.count(word)
                wordcounts[word] = count + wordcounts[word]
        for word in wordcounts:
            wordcounts[word] = wordcounts[word] / len(user.messages) * 100
        user.averageWordcounts = wordcounts

    def kMeansUserClustering(self, users):
        df = DataFrame(columns=("user", "time", "freq", "length"))
        i = 0
        for name, user in users.items():
            df.loc[i] = [user.username, user.timeframe.days, user.frequency, user.lengthAvg]
            i += 1
        model = KMeans(n_clusters=4)
        data_scaled = preprocessing.scale(df[["time", "freq", "length"]].values)
        model.fit(data_scaled)
        df['cluster'] = model.labels_
        print(df)

        # print figures
        fig = plt.figure(1, figsize=(4, 3))
        plt.clf()
        ax = Axes3D(fig, rect=[0, 0, .95, 1], elev=48, azim=134)
        plt.cla()
        ax.scatter(data_scaled[:, 0], data_scaled[:, 1], data_scaled[:, 2], c=model.labels_.astype(np.float))
        ax.w_xaxis.set_ticklabels([])
        ax.w_yaxis.set_ticklabels([])
        ax.w_zaxis.set_ticklabels([])
        ax.set_xlabel('time')
        ax.set_ylabel('freq')
        ax.set_zlabel('length')
        plt.show()

        return df

    def kMeansUserClusteringLabels(self, users):
        listVariabes = ['frequency','"test"','XYZ']
        df = DataFrame(columns=("user", listVariabes[0], listVariabes[1], listVariabes[2]))
        i = 0
        for name, user in users.items():
            if (not user.username == 'developer senior mleitner' and not user.username == 'agligor') and ('developer' in user.username or 'tester' in user.username):
                #username, timeframe.days, frequency, lengthAvg, additions, deletions, modifications, wordcounts["word"], averageWordcounts["word"]
                    #"tests", "tested", "test", "fixed", "added", "automated", "case", "resource", "module", "bug", "elements", "error", "library", "function", "updated", "update", "encoding", "cleanup", "exception", "upgrade", "upgraded"]
                word = "exception"
                #len(user.additions)/(len(user.additions)+len(user.deletions)+len(user.modifications)) len(user.modifications)/(user.timeframe.days+1), (user.timeframe.days+1), len(user.modifications)/len(user.messages)
                df.loc[i] = [getattr(user, 'username'), user.frequency, user.averageWordcounts["test"], len(user.additions)/(len(user.additions)+len(user.deletions)+len(user.modifications))]
            i += 1
        model = KMeans(n_clusters=4)
        data_scaled = preprocessing.scale(df[[listVariabes[0], listVariabes[1], listVariabes[2]]].values)
        model.fit(data_scaled)
        df['cluster'] = model.labels_
        print(df)

        fig = pylab.figure()
        ax = fig.add_subplot(111, projection = '3d')
        sc = ax.scatter(data_scaled[:, 0], data_scaled[:, 1], data_scaled[:, 2], c=model.labels_.astype(np.float))

        #set tickLabels
        xLabels = [item.get_text() for item in ax.get_xticklabels()]
        for i in range(0, len(xLabels)):
            if i == 0:
                xLabels[i] = '0'
            else:
                if i == len(xLabels)-1:
                    xLabels[i] = '1'
                else:
                    xLabels[i] = ''
        ax.set_xticklabels(xLabels)
        yLabels = [item.get_text() for item in ax.get_yticklabels()]
        for i in range(0, len(yLabels)):
            if i == 0:
                yLabels[i] = '0'
            else:
                if i == len(yLabels)-1:
                    yLabels[i] = '1'
                else:
                    yLabels[i] = ''
        ax.set_yticklabels(yLabels)
        zLabels = [item.get_text() for item in ax.get_zticklabels()]
        for i in range(0, len(zLabels)):
            if i == 0:
                zLabels[i] = '0'
            else:
                if i == len(zLabels)-1:
                    zLabels[i] = '1'
                else:
                    zLabels[i] = ''
        ax.set_zticklabels(zLabels)

        #set axisLabels
        ax.set_xlabel(listVariabes[0])
        ax.set_ylabel(listVariabes[1])
        ax.set_zlabel(listVariabes[2])

        #set pointLabels
        global labels_and_points
        labels_and_points = []

        for txt, x, y, z in zip(df['user'], data_scaled[:, 0], data_scaled[:, 1], data_scaled[:, 2]):
            x2, y2, _ = proj3d.proj_transform(x,y,z, ax.get_proj())
            label = pylab.annotate(
                txt,
                xy = (x2, y2), xytext = (-5, 5),
                textcoords = 'offset points', ha = 'right', va = 'bottom',
                bbox = dict(boxstyle = 'round,pad=0', fc = 'yellow', alpha = 0.1),
                arrowprops = dict(arrowstyle = '->', connectionstyle = 'arc3,rad=0'))
            labels_and_points.append((label, x, y, z))

        def update_position(e):
            for label, x, y, z in labels_and_points:
                x2, y2, _ = proj3d.proj_transform(x, y, z, ax.get_proj())
                label.xy = x2,y2
                label.update_positions(fig.canvas.renderer)
            fig.canvas.draw()

        fig.canvas.mpl_connect('button_release_event', update_position)

        pylab.show()

        return df

    def kMeansUserClusteringSimpleInfinica(self, users):
        listVariabes = ['frequency','tests']
        df = DataFrame(columns=("user", listVariabes[0], listVariabes[1]))
        i = 0
        for name, user in users.items():
            if ('developer' in user.username or 'tester' in user.username or 'ss' in user.username) and ('tester ie' not in user.username and 'developer senior mleitner' not in user.username):
                #username, timeframe.days, frequency, lengthAvg, additions, deletions, modifications, wordcounts["word"], averageWordcounts["word"]
                    #"tests", "tested", "test", "fixed", "added", "automated", "case", "resource", "module", "bug", "elements", "error", "library", "function", "updated", "update", "encoding", "cleanup", "exception", "upgrade", "upgraded"]
                #len(user.additions)/(len(user.additions)+len(user.deletions)+len(user.modifications)) len(user.modifications)/(user.timeframe.days+1), (user.timeframe.days+1), len(user.modifications)/len(user.messages)
                df.loc[i] = [user.username, user.frequency, user.averageWordcounts['test']+user.averageWordcounts['tests']+user.averageWordcounts['tested']+user.averageWordcounts['testing']]
            i += 1
        model = KMeans(n_clusters=4)
        data_scaled = preprocessing.scale(df[[listVariabes[0], listVariabes[1]]].values)
        model.fit(data_scaled)
        df['cluster'] = model.labels_
        print(df)
        data_scaled = df[[listVariabes[0], listVariabes[1]]].values

        rangeDf = range(1, len(model.labels_))

        dfc0 = DataFrame(columns=("x", "y"))
        dfc1 = DataFrame(columns=("x", "y"))
        dfc2 = DataFrame(columns=("x", "y"))
        dfc3 = DataFrame(columns=("x", "y"))
        c0i = 0
        c1i = 0
        c2i = 0
        c3i = 0

        for i in rangeDf:
            if model.labels_[i] == 3:
                dfc0.loc[c0i] = [data_scaled[i, 0], data_scaled[i, 1]]
                c0i += 1
            if model.labels_[i] == 1:
                dfc1.loc[c1i] = [data_scaled[i, 0], data_scaled[i, 1]]
                c1i += 1
            if model.labels_[i] == 0:
                dfc2.loc[c2i] = [data_scaled[i, 0], data_scaled[i, 1]]
                c2i += 1
            if model.labels_[i] == 2:
                dfc3.loc[c3i] = [data_scaled[i, 0], data_scaled[i, 1]]
                c3i += 1

        print(dfc0)
        print(dfc1)
        print(dfc2)
        print(dfc3)
        print(str(c0i)+' '+str(c1i)+' '+str(c2i)+' '+str(c3i))

        plt.scatter(dfc0["x"], dfc0["y"], c="1.0", s=50, label="frequent committers", marker='s')
        plt.scatter(dfc1["x"], dfc1["y"], c="0.66", s=50, label="heavy commits", marker='o')
        plt.scatter(dfc2["x"], dfc2["y"], c="0.33", s=50, label="technical testers", marker='v')
        plt.scatter(dfc3["x"], dfc3["y"], c="0.0", s=50, label="non technical testers", marker='d')

        # print figures
#        plt.scatter(data_scaled[:, 0], data_scaled[:, 1], c="0.7", s=50, label="tester", marker='s')

        plt.xlabel('frequency (no. commits per month)')
        plt.ylabel('% of commits including test keywords')
        plt.xlim(-2)
        plt.ylim(-0.5)
        plt.legend(loc='upper right')

#        for text, x, y, in zip(df['user'], data_scaled[:, 0], data_scaled[:, 1]):
 #           plt.annotate(
  #              text,
   #             xy = (x, y), xytext = (-5, 5),
    #            textcoords = 'offset points', ha = 'right', va = 'bottom',
     #           bbox = dict(boxstyle = 'round,pad=0.1', fc = 'yellow', alpha = 0.1),
       #         arrowprops = dict(arrowstyle = '->', connectionstyle = 'arc3,rad=0')
        #    )

        plt.show()

        return df

    def kMeansUserClusteringDecisionTreeInfinica(self, users):
        listVariabes = ['frequency','"tests"']
        df = DataFrame(columns=("user", listVariabes[0], listVariabes[1]))
        i = 0
        for name, user in users.items():
            if ('developer' in user.username or 'tester' in user.username or 'ss' in user.username) and ('tester ie' not in user.username and 'developer senior mleitner' not in user.username):
                #username, timeframe.days, frequency, lengthAvg, additions; deletions, modifications, wordcounts["word"], averageWordcounts["word"]
                df.loc[i] = [user.username, user.frequency, user.averageWordcounts["test"]+user.averageWordcounts["tests"]+user.averageWordcounts["tested"]+user.averageWordcounts["testing"]]
            i += 1

        model = KMeans(n_clusters=4)
        data_scaled = preprocessing.scale(df[[listVariabes[0], listVariabes[1]]].values)
        model.fit(data_scaled)
        df['cluster'] = model.labels_

        y = df['cluster']
        x = df[[listVariabes[0], listVariabes[1]]]

        clf = DecisionTreeClassifier()
        clf = clf.fit(x, y)
        tree.export_graphviz(clf, feature_names=listVariabes, filled=True, rounded=True, special_characters=True)
        with open("test1.dot", 'w') as f:
            f = tree.export_graphviz(clf, out_file=f)

        os.unlink('test1.dot')

        return clf

    def kMeansUserClusteringSimpleProm(self, users):
        listVariabes = ['frequency','tests']
        df = DataFrame(columns=("user", listVariabes[0], listVariabes[1]))
        i = 0
        for name, user in users.items():
            if 'bvazquez' not in user.username:
                #username, timeframe.days, frequency, lengthAvg, additions, deletions, modifications, wordcounts["word"], averageWordcounts["word"]
                    #"tests", "tested", "test", "fixed", "added", "automated", "case", "resource", "module", "bug", "elements", "error", "library", "function", "updated", "update", "encoding", "cleanup", "exception", "upgrade", "upgraded"]
                #len(user.additions)/(len(user.additions)+len(user.deletions)+len(user.modifications)) len(user.modifications)/(user.timeframe.days+1), (user.timeframe.days+1), len(user.modifications)/len(user.messages)
                df.loc[i] = [user.username, user.frequency, user.averageWordcounts['test']+user.averageWordcounts['tests']+user.averageWordcounts['tested']+user.averageWordcounts['testing']]
            i += 1
        model = KMeans(n_clusters=4)
        data_scaled = preprocessing.scale(df[[listVariabes[0], listVariabes[1]]].values)
        model.fit(data_scaled)
        df['cluster'] = model.labels_
        print(df)
        data_scaled = df[[listVariabes[0], listVariabes[1]]].values

        rangeDf = range(1, len(model.labels_))

        dfc0 = DataFrame(columns=("x", "y"))
        dfc1 = DataFrame(columns=("x", "y"))
        dfc2 = DataFrame(columns=("x", "y"))
        dfc3 = DataFrame(columns=("x", "y"))
        c0i = 0
        c1i = 0
        c2i = 0
        c3i = 0

        for i in rangeDf:
            if model.labels_[i] == 0:
                dfc0.loc[c0i] = [data_scaled[i, 0], data_scaled[i, 1]]
                c0i += 1
            if model.labels_[i] == 1:
                dfc1.loc[c1i] = [data_scaled[i, 0], data_scaled[i, 1]]
                c1i += 1
            if model.labels_[i] == 2:
                dfc2.loc[c2i] = [data_scaled[i, 0], data_scaled[i, 1]]
                c2i += 1
            if model.labels_[i] == 3:
                dfc3.loc[c3i] = [data_scaled[i, 0], data_scaled[i, 1]]
                c3i += 1

        print(dfc0)
        print(dfc1)
        print(dfc2)
        print(dfc3)
        print(str(c0i)+' '+str(c1i)+' '+str(c2i)+' '+str(c3i))

        plt.scatter(dfc0["x"], dfc0["y"], c="1.0", s=50, label="core developers", marker='s')
        plt.scatter(dfc1["x"], dfc1["y"], c="0.66", s=50, label="engaged developers", marker='o')
        plt.scatter(dfc2["x"], dfc2["y"], c="0.33", s=50, label="one-time developers", marker='v')
        plt.scatter(dfc3["x"], dfc3["y"], c="0.0", s=50, label="testers", marker='d')

        # print figures
#        plt.scatter(data_scaled[:, 0], data_scaled[:, 1], c="0.7", s=50, label="tester", marker='s')

        plt.xlabel('frequency (no. commits per month)')
        plt.ylabel('% of commits including test keywords')
        plt.xlim(-2)
        plt.ylim(-0.5)
        plt.legend(loc='upper right')

#        for text, x, y, in zip(df['user'], data_scaled[:, 0], data_scaled[:, 1]):
 #           plt.annotate(
  #              text,
   #             xy = (x, y), xytext = (-5, 5),
    #            textcoords = 'offset points', ha = 'right', va = 'bottom',
     #           bbox = dict(boxstyle = 'round,pad=0.1', fc = 'yellow', alpha = 0.1),
       #         arrowprops = dict(arrowstyle = '->', connectionstyle = 'arc3,rad=0')
        #    )

        plt.show()

        return df

    def kMeansUserClusteringDecisionTreeProm(self, users):
        listVariabes = ['frequency','"tests"']
        df = DataFrame(columns=("user", listVariabes[0], listVariabes[1]))
        i = 0
        for name, user in users.items():
            if 'bvazquez' not in user.username:
                #username, timeframe.days, frequency, lengthAvg, additions; deletions, modifications, wordcounts["word"], averageWordcounts["word"]
                df.loc[i] = [user.username, user.frequency, user.averageWordcounts["test"]+user.averageWordcounts["tests"]+user.averageWordcounts["tested"]+user.averageWordcounts["testing"]]
            i += 1

        model = KMeans(n_clusters=4)
        data_scaled = preprocessing.scale(df[[listVariabes[0], listVariabes[1]]].values)
        model.fit(data_scaled)
        df['cluster'] = model.labels_

        y = df['cluster']
        x = df[[listVariabes[0], listVariabes[1]]]
        clf = DecisionTreeClassifier()
        clf = clf.fit(x, y)
        tree.export_graphviz(clf, feature_names=listVariabes, filled=True, rounded=True, special_characters=True)
        with open("test1.dot", 'w') as f:
            f = tree.export_graphviz(clf, out_file=f)

        os.unlink('test1.dot')

        return clf

    def kMeansUserClusteringSimpleCamunda(self, users):
        listVariabes = ['frequency','tests']
        df = DataFrame(columns=("user", listVariabes[0], listVariabes[1]))
        i = 0
        for name, user in users.items():
            if True:
                #username, timeframe.days, frequency, lengthAvg, additions, deletions, modifications, wordcounts["word"], averageWordcounts["word"]
                    #"tests", "tested", "test", "fixed", "added", "automated", "case", "resource", "module", "bug", "elements", "error", "library", "function", "updated", "update", "encoding", "cleanup", "exception", "upgrade", "upgraded"]
                #len(user.additions)/(len(user.additions)+len(user.deletions)+len(user.modifications)) len(user.modifications)/(user.timeframe.days+1), (user.timeframe.days+1), len(user.modifications)/len(user.messages)
                df.loc[i] = [user.username, user.frequency, user.averageWordcounts['test']+user.averageWordcounts['tests']+user.averageWordcounts['tested']+user.averageWordcounts['testing']]
            i += 1
        model = KMeans(n_clusters=4)
        data_scaled = preprocessing.scale(df[[listVariabes[0], listVariabes[1]]].values)
        model.fit(data_scaled)
        df['cluster'] = model.labels_
        print(df)
        data_scaled = df[[listVariabes[0], listVariabes[1]]].values

        rangeDf = range(1, len(model.labels_))

        dfc0 = DataFrame(columns=("x", "y"))
        dfc1 = DataFrame(columns=("x", "y"))
        dfc2 = DataFrame(columns=("x", "y"))
        dfc3 = DataFrame(columns=("x", "y"))
        c0i = 0
        c1i = 0
        c2i = 0
        c3i = 0

        for i in rangeDf:
            if model.labels_[i] == 3:
                dfc0.loc[c0i] = [data_scaled[i, 0], data_scaled[i, 1]]
                c0i += 1
            if model.labels_[i] == 0:
                dfc1.loc[c1i] = [data_scaled[i, 0], data_scaled[i, 1]]
                c1i += 1
            if model.labels_[i] == 2:
                dfc2.loc[c2i] = [data_scaled[i, 0], data_scaled[i, 1]]
                c2i += 1
            if model.labels_[i] == 1:
                dfc3.loc[c3i] = [data_scaled[i, 0], data_scaled[i, 1]]
                c3i += 1

        print(dfc0)
        print(dfc1)
        print(dfc2)
        print(dfc3)
        print(str(c0i)+' '+str(c1i)+' '+str(c2i)+' '+str(c3i))

        plt.scatter(dfc0["x"], dfc0["y"], c="1.0", s=50, label="core developers", marker='s')
        plt.scatter(dfc1["x"], dfc1["y"], c="0.66", s=50, label="engaged developers", marker='o')
        plt.scatter(dfc2["x"], dfc2["y"], c="0.33", s=50, label="one-time developers", marker='v')
        plt.scatter(dfc3["x"], dfc3["y"], c="0.0", s=50, label="testers", marker='d')

        # print figures
#        plt.scatter(data_scaled[:, 0], data_scaled[:, 1], c="0.7", s=50, label="tester", marker='s')

        plt.xlabel('frequency (no. commits per month)')
        plt.ylabel('% of commits including test keywords')
        plt.xlim(-0.5)
        plt.ylim(-3.5)
        plt.legend(loc='upper right')

#        for text, x, y, in zip(df['user'], data_scaled[:, 0], data_scaled[:, 1]):
 #           plt.annotate(
  #              text,
   #             xy = (x, y), xytext = (-5, 5),
    #            textcoords = 'offset points', ha = 'right', va = 'bottom',
     #           bbox = dict(boxstyle = 'round,pad=0.1', fc = 'yellow', alpha = 0.1),
       #         arrowprops = dict(arrowstyle = '->', connectionstyle = 'arc3,rad=0')
        #    )

        plt.show()

        return df

    def kMeansUserClusteringDecisionTreeCamunda(self, users):
        listVariabes = ['frequency','"tests"']
        df = DataFrame(columns=("user", listVariabes[0], listVariabes[1]))
        i = 0
        for name, user in users.items():
            if True:
                #username, timeframe.days, frequency, lengthAvg, additions; deletions, modifications, wordcounts["word"], averageWordcounts["word"]
                df.loc[i] = [user.username, user.frequency, user.averageWordcounts["test"]+user.averageWordcounts["tests"]+user.averageWordcounts["tested"]+user.averageWordcounts["testing"]]
            i += 1

        model = KMeans(n_clusters=4)
        data_scaled = preprocessing.scale(df[[listVariabes[0], listVariabes[1]]].values)
        model.fit(data_scaled)
        df['cluster'] = model.labels_

        y = df['cluster']
        x = df[[listVariabes[0], listVariabes[1]]]
        clf = DecisionTreeClassifier()
        clf = clf.fit(x, y)
        tree.export_graphviz(clf, feature_names=listVariabes, filled=True, rounded=True, special_characters=True)
        with open("test1.dot", 'w') as f:
            f = tree.export_graphviz(clf, out_file=f)

        os.unlink('test1.dot')

        return clf

    def kMeansUserClusteringSimplePredict(self, usersA, usersB):
        listVariabes = ['frequency','tests']
        dfA = DataFrame(columns=("user", listVariabes[0], listVariabes[1]))
        dfB = DataFrame(columns=("user", listVariabes[0], listVariabes[1]))
        i = 0
        for name, user in usersA.items():
            if ('developer' in user.username or 'tester' in user.username or 'ss' in user.username) and ('tester ie' not in user.username and 'developer senior mleitner' not in user.username):
                #username, timeframe.days, frequency, lengthAvg, additions, deletions, modifications, wordcounts["word"], averageWordcounts["word"]
                    #"tests", "tested", "test", "fixed", "added", "automated", "case", "resource", "module", "bug", "elements", "error", "library", "function", "updated", "update", "encoding", "cleanup", "exception", "upgrade", "upgraded"]
                #len(user.additions)/(len(user.additions)+len(user.deletions)+len(user.modifications)) len(user.modifications)/(user.timeframe.days+1), (user.timeframe.days+1), len(user.modifications)/len(user.messages)
                dfA.loc[i] = [user.username, user.frequency, user.averageWordcounts['test']+user.averageWordcounts['tests']+user.averageWordcounts['tested']+user.averageWordcounts['testing']]
            i += 1
        j = 0
        for name, user in usersB.items():
            if 'bvazquez' not in user.username:
                #username, timeframe.days, frequency, lengthAvg, additions, deletions, modifications, wordcounts["word"], averageWordcounts["word"]
                    #"tests", "tested", "test", "fixed", "added", "automated", "case", "resource", "module", "bug", "elements", "error", "library", "function", "updated", "update", "encoding", "cleanup", "exception", "upgrade", "upgraded"]
                #len(user.additions)/(len(user.additions)+len(user.deletions)+len(user.modifications)) len(user.modifications)/(user.timeframe.days+1), (user.timeframe.days+1), len(user.modifications)/len(user.messages)
                dfB.loc[j] = [user.username, user.frequency, user.averageWordcounts['test']+user.averageWordcounts['tests']+user.averageWordcounts['tested']+user.averageWordcounts['testing']]
            j += 1

        data_scaledA = (dfA[[listVariabes[0], listVariabes[1]]].values)
        data_scaledB = (dfB[[listVariabes[0], listVariabes[1]]].values)

        model = KMeans(n_clusters=4)

        model.fit(data_scaledA)
        dfA['cluster'] = model.labels_
        print(dfA)

        dfB['cluster'] = model.predict(data_scaledB)
        print(dfB)

        rangeDf = range(0, len(dfB['cluster']-1))

        dfc0 = DataFrame(columns=("x", "y"))
        dfc1 = DataFrame(columns=("x", "y"))
        dfc2 = DataFrame(columns=("x", "y"))
        dfc3 = DataFrame(columns=("x", "y"))
        c0i = 0
        c1i = 0
        c2i = 0
        c3i = 0

        dfB.reset_index(drop=True)
        data_scaled = dfB[[listVariabes[0], listVariabes[1]]].values

        print(dfB)

        for i in rangeDf:
            try:
                if dfB['cluster'].loc[i] == 0:
                    dfc0.loc[c0i] = [data_scaled[i, 0], data_scaled[i, 1]]
                    c0i += 1
                if dfB['cluster'][i] == 1:
                    dfc1.loc[c1i] = [data_scaled[i, 0], data_scaled[i, 1]]
                    c1i += 1
                if dfB['cluster'][i] == 2:
                    dfc2.loc[c2i] = [data_scaled[i, 0], data_scaled[i, 1]]
                    c2i += 1
                if dfB['cluster'][i] == 3:
                    dfc3.loc[c3i] = [data_scaled[i, 0], data_scaled[i, 1]]
                    c3i += 1
            except: # catch *all* exceptions
                i += 1

        #print(dfc0)
        #print(dfc1)
        #print(dfc2)
        #print(dfc3)
        #print(str(c0i)+' '+str(c1i)+' '+str(c2i)+' '+str(c3i))

        plt.scatter(dfc0["x"], dfc0["y"], c="1.0", s=50, label="core developers", marker='s')
        plt.scatter(dfc1["x"], dfc1["y"], c="0.66", s=50, label="engaged developers", marker='o')
        plt.scatter(dfc2["x"], dfc2["y"], c="0.33", s=50, label="one-time developers", marker='v')
        plt.scatter(dfc3["x"], dfc3["y"], c="0.0", s=50, label="testers", marker='d')

        # print figures
        #        plt.scatter(data_scaled[:, 0], data_scaled[:, 1], c="0.7", s=50, label="tester", marker='s')

        plt.xlabel('frequency (no. commits per month)')
        plt.ylabel('% of commits including test keywords')
        plt.xlim(-0.5)
        plt.ylim(-3.5)
        plt.legend(loc='upper right')

        #        for text, x, y, in zip(df['user'], data_scaled[:, 0], data_scaled[:, 1]):
        #           plt.annotate(
        #              text,
        #             xy = (x, y), xytext = (-5, 5),
        #            textcoords = 'offset points', ha = 'right', va = 'bottom',
        #           bbox = dict(boxstyle = 'round,pad=0.1', fc = 'yellow', alpha = 0.1),
        #         arrowprops = dict(arrowstyle = '->', connectionstyle = 'arc3,rad=0')
        #    )

        plt.show()

        return dfB

class CommitEvaluator:

    symbols = [".", ":", "/", "\\", ",", ";" , "-", "@", "#", "(", ")"]
    mapping = {}

    def __init__(self):
        self.mapping[CommitType.test] = ["test", "tested", "testing", "tests", "testcase", "testcases", ["test", "case"],
                                ["test", "cases"], "unittest", ["unit", "test"], "integrationtest", ["integration", "test"], "fitnesse"]
        self.mapping[CommitType.documentation] = ["documentation", "documented", "javadoc", "readme", "userguide", ["user", "guide"],
                                "tutorial", "faq", "translation", "translate", "translated", "translating", "doc", "i18n"]
        self.mapping[CommitType.maintenance] = ["bugfix", "bugfixes", "fix", "fixed", "fixes", "fixing", "patch", "patched", "cleanup",
                                "cleanups", ["clean", "up"], "clean", "cleaned", "cleaning"]
        self.mapping[CommitType.development] = ["implement", "implemented", "implementation", "implementing", "implements", "improved",
                                "improving", "update", "updated", "updating", "script", "scripting"]
        self.mapping[CommitType.refactor] = ["refactor", "refact", "refactored", "refactoring", "refactorings", "rename", "renamed", "renaming",
                                "move", "moved", "revert", "reverted", "reverting"]
        self.mapping[CommitType.toolManagement] = ["tool", "library", "libraries", "framework", "dependency", "dependencies",
                                ["upgrade", "to"], ["upgrade", "to"]]
        self.mapping[CommitType.backend] = ["engine"]
        self.mapping[CommitType.removal] = ["delete", "deleted", "deleting", "remove", "removed", "removing"]
        self.mapping[CommitType.addition] = ["added", "add", "adding", "new", "create", "created"]
        self.mapping[CommitType.design] = ["style", "styles", "styling", "icon", "icons", "font", "layout", "layouts", "layouted",
                                "layouting"]
        self.mapping[CommitType.build] = ["build", "building", "compile", "compiled", "compiling", "release"]
        self.mapping[CommitType.web] = ["web", "spring", "http", "https", "rest", "html", "css", "servlet"]
        self.mapping[CommitType.vcsManagement] = ["svn", "git", "mercurial"]
        self.mapping[CommitType.data] = ["data", "database", "sql", "postgres", "postgre", "postgresql"]


    def countWords(self, commits):
        messages = ""
        for commit in commits:
            messages = messages + " " + commit.message
        tokens = nltk.word_tokenize(messages)
        dist = nltk.FreqDist(tokens)
        return dist

    def classifyCommits(self, commits):
        typeMap = {CommitType.other: 0}
        for i in range(0, len(commits)):
            commit = commits[i]
            commitTypes = set(self.classify(commit))
            commit.types = commitTypes
            if len(commitTypes) == 0:
                typeMap[CommitType.other] += 1
            else:
                for type in commitTypes:
                    if type in typeMap:
                        typeMap[type] = typeMap[type] + 1
                    else:
                        typeMap[type] = 1
        return typeMap

    def classify(self, commit):
        if commit.merge:
            return [CommitType.merge]
        if commit.message.startswith("Automated Release") or commit.message.startswith("Automated Nightly"):
            return [CommitType.automated]
        commitTypes = self.classifyByMessage(commit.message)
        files = []
        files.extend(commit.additions)
        files.extend(commit.modifications)
        files.extend(commit.deletions)
        commitTypes = self.classifyByTypes(files, commitTypes)
        commitTypes = self.classifyByOperations(commit.additions, commit.modifications, commit.deletions, commitTypes)
        return commitTypes

    def classifyByMessage(self, text):
        commitTypes = []
        words = []
        tokens = nltk.word_tokenize(text)
        for token in tokens:
            add = True
            for symbol in self.symbols:
                if token == symbol:
                    add = False
                    break
                elif symbol in token:
                    add = False
                    split = token.split(symbol)
                    for item in split:
                        if item.strip() != "":
                            tokens.append(item)
            if add:
                words.append(token.lower())

        if len(words) == 0:
            return [CommitType.empty]

        for commitType, wordList in self.mapping.items():
            if self.occurrs(words, wordList):
                commitTypes.append(commitType)

        return commitTypes

    def occurrs(self, words, checkList):
        for item in checkList:
            if isinstance(item, list):
                for i in range(0, len(words)-1):
                    if words[i] == item[0]:
                        fit = True
                        for j in range(0, len(item)):
                            if item[j] != words[i+j]:
                                fit = False
                        if fit:
                            return True
            else:
                if item in words:
                    return True
        return False

    def startsWith(self, words, checkList):
        for item in checkList:
            if isinstance(item, list):
                for i in range(0, len(item)-1):
                    fit = True
                    if item[i] != words[i]:
                        fit = False
                    if fit:
                        return True
            else:
                if words[0] == item:
                    return True
        return False

    def compare(self, word1, word2):

        self.lemmatizer.lemmatize()

    def classifyByTypes(self, files, commitTypes):
        doc = False
        web = False
        design = False
        for file in files:
            if self.equalToAny(file, ["adoc", "txt", "doc", "docx", "text", "tex", "pdf"]):
                doc = True
            elif self.equalToAny(file, ["html", "css", "js", "jsp", "json"]):
                web = True
            elif self.equalToAny(file, ["png", "svg", "jpg"]):
                design = True
        if doc:
            commitTypes.append(CommitType.documentation)
        if web:
            commitTypes.append(CommitType.web)
        if design:
            commitTypes.append(CommitType.design)

        return commitTypes

    def equalToAny(self, text, candidates):
        for candidate in candidates:
            if text == candidate:
                return True
        return False

    def classifyByOperations(self, additions, modifications, deletions, commitTypes):
        addCount = len(additions)
        modCount = len(modifications)
        delCount = len(deletions)
        if addCount > 0 and modCount == 0 and delCount == 0:
            commitTypes.append(CommitType.addition)
        elif delCount > 0 and modCount == 0 and addCount == 0:
            commitTypes.append(CommitType.removal)
        return commitTypes

class User:
    def __init__(self, username):
        self.username = username
        self.messages = []
        self.timestamps = []
        self.additions = []
        self.modifications = []
        self.deletions = []
        self.types = {}

    def addTimestamp(self, timestamp):
        self.timestamps.append(timestamp)

    def addMessage(self, message):
        self.messages.append(message)

    def addAdditions(self, additions):
        self.additions += additions

    def addModifications(self, modifications):
        self.modifications += modifications

    def addDeletions(self, deletions):
        self.deletions += deletions

class CommitType(Enum):
    test = "test"
    development = "development"
    refactor = "refactor"
    web = "web"
    frontend = "frontend"
    backend = "backend"
    design = "design"
    addition = "addition"
    removal = "removal"
    maintenance = "maintenance"
    documentation = "documentation"
    architecture = "architecture"
    toolManagement = "toolManagement"
    build = "build"
    data = "data"

    vcsManagement = "vcsManagement"
    merge = "merge"
    automated = "automated"
    other = "other"
    empty = "empty"